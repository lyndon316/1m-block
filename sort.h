
#include <stdlib.h>
#include <string.h>
int strcmp2(const void* a, const void* b)
{
	const char* str1 = *(const char**)a;
	const char* str2 = *(const char**)b;
	return strcmp(str1, str2);
}

void str_sort(char** strs, int strs_len)
{
	qsort(strs, strs_len, sizeof(strs[0]), strcmp2);
}

/*

void dump2(unsigned char* buf, int size) {
	int i;
	for (i = 0; i < size; i++) {
		if (i != 0 && i % 16 == 0)
			printf("\n");
		printf("%02X ", buf[i]);
	}
	printf("\n");
}
*/


int bin_search(char** strs, int strs_len, char* target)
{
	int L, H, M;
	int str_len;
	L = 0;
	H = strs_len - 1;
	//dump2((unsigned char*)target, 28);
	while(L <= H)
	{
		M = (L + H) / 2;
		//dump2((unsigned char*)strs[M], 28);
		//printf("%s\n", strs[M]);
		str_len = strlen(strs[M]) - 1;
		if(memcmp(target, strs[M], str_len) == 0)
		{
			return M;
		}
		else if(memcmp(target, strs[M], str_len) < 0)
		{
			H = M - 1;
		}
		else
		{
			L = M + 1;
		}
	}
	return -1;
}