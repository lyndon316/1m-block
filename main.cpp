#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */
#include <errno.h>
#include <cstring>
#include <signal.h>
#include "sort.h"

#include <libnetfilter_queue/libnetfilter_queue.h>

#define LIBNET_LIL_ENDIAN 1
typedef struct libnet_ipv4_hdr
{
#if (LIBNET_LIL_ENDIAN)
    u_int8_t ip_hl:4,      /* header length */
           ip_v:4;         /* version */
#endif
#if (LIBNET_BIG_ENDIAN)
    u_int8_t ip_v:4,       /* version */
           ip_hl:4;        /* header length */
#endif
    u_int8_t ip_tos;       /* type of service */
#ifndef IPTOS_LOWDELAY
#define IPTOS_LOWDELAY      0x10
#endif
#ifndef IPTOS_THROUGHPUT
#define IPTOS_THROUGHPUT    0x08
#endif
#ifndef IPTOS_RELIABILITY
#define IPTOS_RELIABILITY   0x04
#endif
#ifndef IPTOS_LOWCOST
#define IPTOS_LOWCOST       0x02
#endif
    u_int16_t ip_len;         /* total length */
    u_int16_t ip_id;          /* identification */
    u_int16_t ip_off;
#ifndef IP_RF
#define IP_RF 0x8000        /* reserved fragment flag */
#endif
#ifndef IP_DF
#define IP_DF 0x4000        /* dont fragment flag */
#endif
#ifndef IP_MF
#define IP_MF 0x2000        /* more fragments flag */
#endif 
#ifndef IP_OFFMASK
#define IP_OFFMASK 0x1fff   /* mask for fragmenting bits */
#endif
    u_int8_t ip_ttl;          /* time to live */
    u_int8_t ip_p;            /* protocol */
    u_int16_t ip_sum;         /* checksum */
    struct in_addr ip_src, ip_dst; /* source and dest address */
}IP_hdr;

/*
 *  TCP header
 *  Transmission Control Protocol
 *  Static header size: 20 bytes
 */
typedef struct libnet_tcp_hdr
{
    u_int16_t th_sport;       /* source port */
    u_int16_t th_dport;       /* destination port */
    u_int32_t th_seq;          /* sequence number */
    u_int32_t th_ack;          /* acknowledgement number */
#if (LIBNET_LIL_ENDIAN)
    u_int8_t th_x2:4,         /* (unused) */
           th_off:4;        /* data offset */
#endif
#if (LIBNET_BIG_ENDIAN)
    u_int8_t th_off:4,        /* data offset */
           th_x2:4;         /* (unused) */
#endif
    u_int8_t  th_flags;       /* control flags */
#ifndef TH_FIN
#define TH_FIN    0x01      /* finished send data */
#endif
#ifndef TH_SYN
#define TH_SYN    0x02      /* synchronize sequence numbers */
#endif
#ifndef TH_RST
#define TH_RST    0x04      /* reset the connection */
#endif
#ifndef TH_PUSH
#define TH_PUSH   0x08      /* push data to the app layer */
#endif
#ifndef TH_ACK
#define TH_ACK    0x10      /* acknowledge */
#endif
#ifndef TH_URG
#define TH_URG    0x20      /* urgent! */
#endif
#ifndef TH_ECE
#define TH_ECE    0x40
#endif
#ifndef TH_CWR   
#define TH_CWR    0x80
#endif
    u_int16_t th_win;         /* window */
    u_int16_t th_sum;         /* checksum */
    u_int16_t th_urp;         /* urgent pointer */
}TCP_hdr;

#define MAX_STR_SIZE 1557
char** blacklist;
int blacklist_len;

void dump(unsigned char* buf, int size) {
	int i;
	for (i = 0; i < size; i++) {
		if (i != 0 && i % 16 == 0)
			printf("\n");
		printf("%02X ", buf[i]);
	}
	printf("\n");
}

void sigint_handler(int sig)
{
	//preventing malloc cat
	printf("SIGINT - stopping...\n");
	for(int i = 0; i < blacklist_len; i++)
	{
		free(blacklist[i]);
	}
	free(blacklist);
	exit(0);
}


int file_lines(char* filename)
{
    FILE* file = fopen(filename, "r");
    if (file == NULL)
    {
        printf("no file to block lol\n");
        return -1;
    }

    int count = 0;
    char temp;
    while ((temp = fgetc(file)) != EOF)
    {
        if (temp == '\n')
        {
            count++;
        }
    }

    fclose(file);
    return count;
}

int getfile(char** strs, char* filename)
{
	FILE* file = fopen(filename, "r");
    if (file == NULL)
    {
        printf("no file to block lol\n");
        return -1;
    }
    char line[MAX_STR_SIZE];
    char* offset;
    int i = 0;
    int malloc_size;
    while (fgets(line, sizeof(line), file) != NULL)
    {
        offset = strstr(line, ",");
        if(offset != NULL)
        {
        	malloc_size = strlen(line) - (offset - line) - 1;
        	strs[i] = (char*)malloc(malloc_size);
        	strncpy(strs[i], offset + 1, malloc_size);
        }
        else
        {
        	printf("why strstr fail sibal\n");
        	return -1;
        }
        i++;
    }
    printf("%s\n", strs[3]);
    return 0;
}


/* returns packet id */
static u_int32_t print_pkt (struct nfq_data *tb)
{
	int id = 0;
	struct nfqnl_msg_packet_hdr *ph;
	struct nfqnl_msg_packet_hw *hwph;
	u_int32_t mark,ifi;
	int payload_len;
	unsigned char *data;

	ph = nfq_get_msg_packet_hdr(tb);
	if (ph) {
		id = ntohl(ph->packet_id);
	}

	payload_len = nfq_get_payload(tb, &data);
	if (payload_len >= 0)
		printf("payload_len=%d\n", payload_len);

	int hdr_size;

	if(((IP_hdr*)data) -> ip_v == 4) //ipv4
	{
		//printf("IPv4\n");
		if(((IP_hdr*)data) -> ip_p != 6)
		{
			//printf("not TCP pkt\n");
		}
		else
		{
			hdr_size = ((IP_hdr*)data) -> ip_hl * 4;
			TCP_hdr* TCP_ptr = (TCP_hdr*)((uint8_t*)data + hdr_size);
			if(ntohs(TCP_ptr -> th_dport) != 80)
			{
				//printf("not port 80\n");
			}
			else
			{
				hdr_size += TCP_ptr -> th_off * 4;
				char* data_ptr = (char*)data + hdr_size;
				//dump((uint8_t*)data + hdr_size, payload_len - hdr_size);
				if(payload_len - hdr_size < 3)
				{
					printf("not enough payload len\n");
					printf("hdr_size : %d\n", hdr_size);
				}
				else if(memcmp(data_ptr, "GET ", 4) == 0 || memcmp(data_ptr, "POST", 4) == 0)
				{	//hello http response
					int host_offset = 0;
					int host_end_offset = 0;
					for(int i = 0; i < payload_len - hdr_size; i++)
					{
						if(*(data_ptr + i) == '\n')
						{
							host_offset = i + 1;
							break;
						}
					}
					if(host_offset == 0)
					{
						printf("cannot find LF in packet\n");
					}	
					else
					{
						do{
							host_end_offset = 0;
							for(int i = 0; i < payload_len - hdr_size - host_offset; i++)
							{
								if(*(data_ptr + host_offset + i) == '\n')
								{
									host_end_offset = i + 1; 
									break;
								}
							}
							if(host_end_offset == 0)
							{
								printf("cannot find LF in packet\n");
							}
							host_offset += host_end_offset;
						}
						while(strncmp(data_ptr + host_offset, "Host: ", 6) != 0 && (host_offset < payload_len - hdr_size));
						
						char* char_ptr = data_ptr + host_offset + 6;
						while(*char_ptr != '\n')
						{
							char_ptr++;
						}
						//memcpy((char*)data + hdr_size + host_offset + 2, "www.", )
						printf("%s\n", data_ptr + host_offset + 6);
						dump((unsigned char*)blacklist[2], 30);
						
						if(bin_search(blacklist, blacklist_len, data_ptr + host_offset + 6) != -1)
						{
							printf("ban this\n");
							id = 0;
						}
						
					}
				}	
				
				fputc('\n', stdout);
			}
		}
	}
	return id;
}


static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
	      struct nfq_data *nfa, void *data)
{
	uint32_t id = print_pkt(nfa);
	int ban = 0;
	//printf("entering callback\n");
	if(id != 0)
	{
		return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
	}
	else
	{
		return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
	}
}

int main(int argc, char **argv)
{
	struct nfq_handle *h;
	struct nfq_q_handle *qh;
	struct nfnl_handle *nh;
	int fd;
	int rv;
	char buf[4096] __attribute__ ((aligned));

	blacklist_len = file_lines(argv[1]);
	signal(SIGINT, sigint_handler);
	blacklist = (char**)malloc(sizeof(char*) * blacklist_len);

	getfile(blacklist, argv[1]);

	printf("Sorting...\n");
	str_sort(blacklist, blacklist_len);
	//dump(blacklist[1], )
	/*
	printf("%s\n%s\n", blacklist[1], blacklist[1234]);
	char* asdf;
	asdf = (char*)malloc(100);
	memcpy(asdf, "google.com", 11);
	printf("%d\n", bin_search(blacklist, blacklist_len, asdf));
	*/
	printf("opening library handle\n");
	h = nfq_open();

	if (!h) {
		fprintf(stderr, "error during nfq_open()\n");
		exit(1);
	}

	printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
	if (nfq_unbind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_unbind_pf()\n");
		exit(1);
	}

	printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
	if (nfq_bind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_bind_pf()\n");
		exit(1);
	}

	printf("binding this socket to queue '0'\n");
	qh = nfq_create_queue(h,  0, &cb, NULL);
	if (!qh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}

	printf("setting copy_packet mode\n");
	if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}

	fd = nfq_fd(h);

	for (;;) {
		if ((rv = recv(fd, buf, sizeof(buf), 0)) >= 0) {
			//printf("pkt received\n");
			nfq_handle_packet(h, buf, rv);
			continue;
		}
		/* if your application is too slow to digest the packets that
		 * are sent from kernel-space, the socket buffer that we use
		 * to enqueue packets may fill up returning ENOBUFS. Depending
		 * on your application, this error may be ignored. nfq_nlmsg_verdict_putPlease, see
		 * the doxygen documentation of this library on how to improve
		 * this situation.
		 */
		if (rv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}

	printf("unbinding from queue 0\n");
	nfq_destroy_queue(qh);

#ifdef INSANE
	/* normally, applications SHOULD NOT issue this command, since
	 * it detaches other programs/sockets from AF_INET, too ! */
	printf("unbinding from AF_INET\n");
	nfq_unbind_pf(h, AF_INET);
#endif

	printf("closing library handle\n");
	nfq_close(h);
	free(blacklist);
	exit(0);
}
