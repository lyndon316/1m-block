LDLIBS=-lnetfilter_queue

all: 1m-block

sort.o: sort.h sort.c

1m-block: main.o sort.o
	$(LINK.cc) $^ $(LOADLIBES) $(LDLIBS) -o $@

clean:
	rm -f 1m-block *.o
